sudo apt-get update
sudo apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
cat >>/etc/docker/daemon.json<<EOF
{
        "log-driver":"json-file",
        "log-opts": { 
                "max-size": "100m", 
                "max-file": "5"
        }
}
EOF
curl -L "https://github.com/docker/compose/releases/download/v2.14.0/docker-compose-linux-x86_64" -o /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose
